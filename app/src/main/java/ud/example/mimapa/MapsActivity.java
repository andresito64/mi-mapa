package ud.example.mimapa;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener {


    private final LatLng SAN = new LatLng(12.587151, -81.698713);
    private LocationManager locationManager;
    private LocationListener locationListener;
    private LatLng miUbicacion;
    boolean isMarket = true;

    private GoogleMap mMap;
//    private ActivityMapsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        locationManager = (LocationManager)
                this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                miUbicacion = new
                        LatLng(location.getLatitude(), location.getLongitude());
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };
        requestPermission();
    }

    public void addMarker(View v) {
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(mMap.getCameraPosition().target.latitude,
                        mMap.getCameraPosition().target.longitude)
        ).icon(BitmapDescriptorFactory.
                defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
    }

    public void moveCamera(View v) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SAN, 15));
    }

    public void goLocation(View v){
        double latitud = Double.parseDouble(((EditText)findViewById(R.id.etLatitud)).getText().toString());
        double longitud = Double.parseDouble(((EditText)findViewById(R.id.etLongitud)).getText().toString());
        mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(latitud, longitud)));
    }

    public void actDescMarcador(View v){
        Button buttonEnable = (Button)v;
        if (isMarket){
            isMarket = false;
            buttonEnable.setText("DESAC MARCADOR");
        }
        else{
            isMarket = true;
            buttonEnable.setText("ACT MARCADOR");
        }
    }


    public void camSatelite(View v) {
        Button buttonType = (Button) v;
        int type = mMap.getMapType();
        switch (type){
            case 0 :
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                buttonType.setText("Normal");
                break;
                case 1 :
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                buttonType.setText("Satelite");
                break;
                case 2 :
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                buttonType.setText("Tierra");
                break;
                case 3 :
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                buttonType.setText("Hibrido");
                break;
                case 4 :
                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                buttonType.setText("Ninguno");
                break;
        }

    }

    public void camNormal(View v) {
        mMap.clear();
    }

    public void mostrarUD(View v) {
        LatLng UD = new LatLng(4.628618, -74.065569);
        Marker miMaker = mMap.addMarker(new MarkerOptions()
                .position(UD)
                .title("UD")
                .snippet("UNIVERSIDAD DISTRITAL"));
        miMaker.showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(UD, 16));
    }

    public void mostrarmiubicacion(View v) {
        try {
            Log.i("OJO: ", miUbicacion.toString());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(miUbicacion, 15));
        } catch (Exception ex) {
        }
    }

    protected void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ActivityCompat.checkSelfPermission
                    (this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission
                    (this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        0, 0, locationListener);
            }
        }
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.setOnMapClickListener((GoogleMap.OnMapClickListener) this);

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (isMarket) {
            mMap.addMarker(new MarkerOptions().position(latLng));
        }
    }

}